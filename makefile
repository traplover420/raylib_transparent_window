linking = -lraylib -lglfw -lGL -lopenal -lm -pthread -ldl
header = -Iheader
object = object/mainwin.o

all: bin/main

object/mainwin.o: src/mainwin.cpp header/mainwin.hpp
	g++ -c src/mainwin.cpp $(header) -o object/mainwin.o

bin/main: $(object)
	g++ $(object) $(linking) -o bin/main

clean:
	rm $(object)
	rm bin/main